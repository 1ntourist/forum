from django.contrib import admin
from .models import *

admin.site.register(Ads)
admin.site.register(Rubric)
admin.site.register(Theme)